#!/usr/bin/env bash
set -ex
KEY=$HOME/.ssh/id_rsa
PUB=${KEY}.pub

test -f $KEY && exit 1
ssh-keygen -b 4096 -t rsa -N "" -f $KEY
echo "Add the following to /backup-tar/$HOSTNAME/.ssh/authorized_keys on the backup server:"
cat $PUB
