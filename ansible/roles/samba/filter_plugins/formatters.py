import jinja2.runtime

def yesno(value, default="no") -> str:
    if isinstance(value, jinja2.runtime.Undefined) or value is None:
        return default
    if value is True:
        return "yes"
    if value is False:
        return "no"
    raise ValueError(f"Value {value} not valid boolean")


def smb_list(value) -> str:
    if isinstance(value, list):
        assert all(map(lambda x: isinstance(x, str), value)), "values must be strings"
        return ", ".join(value)
    if isinstance(value, str):
        return value
    if isinstance(value, jinja2.runtime.Undefined) or value is None:
        return ""
    raise ValueError(f"Value {value} is neither list of strings nor a string (or undefined)")


class FilterModule(object):
    def filters(self):
        return {"yesno": yesno, "smb_list": smb_list}
