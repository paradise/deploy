# Infrastruktura ParaDiSe

## Strutkura repa

- `ansible` – „nový“ deployment přes ansible, většina konfigurace většiny strojů (mimo úplně základní instalace debianu)
- (root), `pkgs`, `conf`, … – historický, custom made deployment archlinuxu, ten už ale zůstal jen na některých desktopech – TODO: smazat

## Stroje

### HW

- `pontos01`–`08` – „nové“ servery AMD Epyc (2 × 16jader × 2HT), 1TB RAM, 2×2TB NVME (Intel)
  - 01–04 home + login, 05–08 bench
  - IPMI v každém stroji + v boxu (tj. pro 4 stroje dohromady)
- `anna` – web server, read-only home, bez IPMI
- `antea` – legacy login server, zálohy
- `arke` – ± nic
- `pheme01`–`21`(`22`) – staré (~ 2010) výpočetní stoje, vypnuté, minimálně jeden nefunkční
- `pythia01`–`10` – labové workstation (~2014)

### Úložiště

Víceméně všechny disky v serverech využívají LVM k správě disků.

- `pontos` – každý ze serverů má 2 × 2TB NVME (2.5") + volné SATA 2.5" sloty (4?)
  - `/home` (pro `pontos01`–`04`) v RAID1 (mirroring), NFS export (v `/export`)
  - `/obj` v LVM RAID0

- `antea` – 4 × 3.5" SAS (1×140GB + 3×2TB)
  - jako jediný server nemá podporu pro SATA, vyžaduje SAS disky
  - historické `/home` v LVM RAID1 na interních discích
  - zálohy na iSCSI importech z THECUSu (↓)

- `anna` – 4 × 3.5" SATA (možná umí i SAS)
  - weby (`/srv/www`), postgres (`/srv/postgres`), SVN (`/srv/code`), home-archive, … – LVM RAID1

- `antea` – 4 × 3.5" SATA (možná umí i SAS)

- THECUS – NAS, iSCSI – 8 × SATA
  - náhrada Promise (↓)
  - připojen 1×1Gb ethernet k antee, případně k anně či jinému serveru
  - statická IP 192.168.0.100 (port k antee), 192.168.1.100 (port k anně)
  - očekává druhý konec jako 192.168.0.10 (antea), resp. 192.168.1.10 (anna)
  - webové rozhraní dostupné ze serveru k němu připojenému (z bezpečnostních důvodů není veřejné)
  - servuje iSCSI pole postavené buď přímo nad disky nebo nad RAID
  - relativně pomalé (obzvlášť při přístupu na více než jedno iSCSI pole paralelně)
  - primárně **zálohy**

- SCSI Promise – historické SATA úložiště, odpojené
  - propojovací SCSI kabel který je lze zapojit do antey nebo anny
  - fyzicky je stále v racku, primárně ale jako „polička“ – leží na něm THECUS, který nemá
    kolejničky
  - v podstatě už nefunkční či velmi nespolehlivý
  - neumí disky větší než 2 TB
  - proprietary HW RAID

## Uživatelé

- přihlásit by se měl moci každý, kdo je ve [fadmin skupině lab_paradise](https://fadmin.fi.muni.cz/auth/people/sprava_skupiny.mpl?skupina_id=99833), k tomu aby byl účet smysluplně použitelný je ale třeba vytvořit home
    - účty se tahají z LDAP, hesla z Kerbera fakulty
- info o uživatelích se většinou přebírá z ldabu ale semi-historitší uživatelé (kteří byli v labu cca 2019) na některých strojích (typicky pontech, které měli debian realtivně brzo a tím pádem jsou vlastně nejdéle nainstalované) mají učty i lokálně v `/etc/passwd`, takže reálně existují i když jim FI účet zmizí (jen se nepřihlásí heslem, protože heslo je v kerberu)

### Home

TODO

### Fadmin skupiny

- `lab_paradise` – přístup na labové stroje
  - kromě ParaDiSe i značná část Formely + část Sybily
- `mail_paradise-supp` – správcovská skupina + mail na notifikace rootům
  - jak interní notifikace, tak od unix@fi, případně dalších vnějších správců
  - určuje kdo spravuje ostatní skupiny ParaDiSe
  - mělo by odpovídat lidem co mají root práva na stroje ParaDiSe
- `mail_paradise` – `paradise@fi.muni.cz` + přístup na gitlab skupiny
  <https://gitlab.fi.muni.cz/paradise>
  - typicky jen členové labu
- `unix_paradise` – historická skupina, pravděpodobně pro použití na ne-labových strojích
- `mail_divine` – `divine@fi.muni.cz`
- `mail_rofi` (?) – `rofi@fi.muni.cz` – není spravovaná paradise-supp

### GitLab

<https://gitlab.fi.muni.cz/paradise>

- synchronizován s `mail_paradise`, `mail_paradise-supp` jsou owners (cca 1× za hodinu)
- primárně interní repozitáře (privátní skupině, případně uvnitř FI) + mirrory GitHubu

### GitHub

<https://github.com/paradise-fi>

- veřejná skupina, jen veřejné repozitáře
- uživatele je třeba přidávat ručně pozvánkou
- správci by měli odpovídat ParaDiSe roots + Honza Mrázek

### Deployment

- ansible

## Zálohy

TODO


<!--
vim: spell spelllang=cs tw=100 colorcolumn=100 fo+=t
-->
